function cards (cardsData,listId,callback){

    if(cardsData instanceof Object && typeof(listId)=='string' && typeof(callback)=='function'){
        setTimeout(()=>{
            const reqCards = cardsData[listId]
            callback(reqCards)
        },2000)
    }
}

module.exports = cards