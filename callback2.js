function list(listData,boardId,callback){
    if(listData instanceof Object && typeof(boardId)=='string' && typeof(callback)=='function'){
    setTimeout(()=>{
        const reqList =listData[boardId]
        callback(reqList)
    },2000)
}}

module.exports = list;