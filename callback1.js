function findBoard(boardsData, id, callback) {
    if (Array.isArray(boardsData) && typeof id == 'string' && typeof callback == 'function') {
        setTimeout(() => {
            const requiredObj = boardsData.find(obj => obj.id == id)

            callback(requiredObj)

        }, 1000)
    }
}

console.log('control returned to code')

module.exports = findBoard;